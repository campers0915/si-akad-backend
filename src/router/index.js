const express = require("express");
const Router = express.Router();

const accountRouter = require("./account");
const academicRouter = require("./academic");
/**
 * - Role Admin
    1. Manage User Guru & Siswa (CRUD)
    2. Manage Tahun Ajaran (CRUD)
    3. Manage Kelas (CRUD)
    4. Manage Mata Pelajaran (CRUD)
    5. Set Mata Pelajaran Guru (CRUD)
    6. Rekap absensi
- Role Guru
    1. Read Jadwal
    2. Create Absensi
    3. Rekap absensi
- Role Siswa
    1. Read Jadwal
    2. Absen
 */
Router.get("/", (_, res) => {
  res.json({
    msg: "Welcome to course API",
  });
});
Router.use("/account", accountRouter);
Router.use("/academic", academicRouter);
// 404
Router.use((_, res) => {
  res.status(404).json({
    msg: "Invalid Route",
  });
});

module.exports = Router;
