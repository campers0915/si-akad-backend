const express = require("express");
const Router = express.Router();

const authController = require("../controller/auth");
const profileController = require("../controller/profile");
const adminController = require("../controller/userAdmin");

// Auth
// Login
Router.post("/auth", authController.login);
// Logout
// Router.delete("/auth", authController.logout);
// Password (Forgot/Edit)
Router.patch("/auth", (req, res) => {
  res.json({ msg: "coming soon" });
});

// Profile
// View
Router.get("/profile", profileController.getProfile);
// Edit
Router.patch("/profile", profileController.editProfile);

// Admin
Router.get("/admin/user/:role", adminController.getAllUserByRole);
Router.get("/admin/user/:role/:user", adminController.getUserById);
Router.post("/admin/user", adminController.createNewUser);
Router.patch("/admin/user/:role/:user", adminController.editUserById);
Router.delete("/admin/user/:role/:user", adminController.deleteUserById);

module.exports = Router;
