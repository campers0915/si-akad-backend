const express = require("express");
const Router = express.Router();

const classController = require("../controller/class");
const subjectController = require("../controller/subject");
const yearController = require("../controller/year");
const scheduleController = require("../controller/schedule");
const presenceController = require("../controller/presence");

// Class
Router.get("/class", classController.getClass);
Router.get("/class/:id", classController.getClassDetail);
Router.post("/class", classController.createNewClass);
Router.patch("/class/:id", classController.editClassDetail);
Router.delete("/class/:id", classController.deleteClass);

// Mapel
Router.get("/subject", subjectController.getSubject);
Router.post("/subject", subjectController.createNewSubject);
Router.patch("/subject/:id", subjectController.editSubjectDetail);
Router.delete("/subject/:id", subjectController.deleteSubject);

// Tahun Ajaran
Router.get("/year", yearController.getYear);
Router.post("/year", yearController.createNewYear);
Router.patch("/year/:id", yearController.editYear);
Router.delete("/year/:id", yearController.deleteYear);

// Jadwal
Router.get("/schedule", scheduleController.getSchedule);
// Router.get("/schedule/:id", scheduleController.getScheduleDetail);
Router.post("/schedule", scheduleController.createNewSchedule);
Router.patch("/schedule/:id", scheduleController.editScheduleDetail);
Router.delete("/schedule/:id", scheduleController.deleteSchedule);

// Absensi
Router.get("/presence", presenceController.getPresence);
Router.post("/presence", presenceController.createNewPresence);
Router.patch("/presence/:id", presenceController.editPresenceDetail);
Router.delete("/presence/:id", presenceController.deletePresence);

module.exports = Router;
