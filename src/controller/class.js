const db = require("../config/mysql");

const getClass = (req, res) => {
  const year = req.query.year || null;
  const semester = req.query.semester || null;
  const classroom = req.query.class || null;
  const values = [];
  let sqlQuery =
    "SELECT k.id, k.nama, t.tahunajar, t.semester FROM kelas k JOIN tahunajar t ON k.tahunAjar_id = t.id";
  if (year || semester || classroom) sqlQuery += " WHERE";
  if (year) {
    sqlQuery += " t.tahunajar = ?";
    values.push(year);
  }
  if (semester) {
    if (values.length > 0) sqlQuery += " AND";
    sqlQuery += " t.semester = ?";
    values.push(semester);
  }
  if (classroom) {
    if (values.length > 0) sqlQuery += " AND";
    sqlQuery += " k.nama LIKE ?";
    values.push(`%${classroom}%`);
  }
  db.query(sqlQuery, values, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    return res.status(200).json({ data: result });
  });
};

const getClassDetail = (req, res) => {
  const sqlQuery =
    "SELECT k.id, k.nama, t.tahunajar, t.semester FROM kelas k JOIN tahunajar t ON k.tahunAjar_id = t.id WHERE k.id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    if (result.length === 0)
      return res.status(404).json({ msg: "Kelas tidak ditemukan" });
    return res.status(200).json({ data: result });
  });
};

const createNewClass = (req, res) => {
  const sqlQuery = "INSERT INTO kelas SET ?";
  db.query(sqlQuery, req.body, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    if (result.affectedRows > 0)
      return res.status(201).json({
        msg: "Penambahan berhasil",
      });
    return res.status(500).json({ msg: "Penambahan gagal" });
  });
};

const editClassDetail = (req, res) => {
  const sqlQuery = "UPDATE kelas SET ? WHERE id = ?";
  db.query(sqlQuery, [req.body, req.params.id], (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Edit berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

const deleteClass = (req, res) => {
  const sqlQuery = "DELETE FROM kelas WHERE id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Hapus berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

module.exports = {
  getClass,
  getClassDetail,
  createNewClass,
  editClassDetail,
  deleteClass,
};
