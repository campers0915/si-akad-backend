const db = require("../config/mysql");

const getSchedule = (req, res) => {
  const day = req.query.day || null;
  const classroom = req.query.class || null;
  const subject = req.query.subject || null;
  const values = [];
  let sqlQuery =
    "SELECT j.id, g.nama AS guru, j.hari, j.jam_mulai, j.jam_selesai, k.nama AS kelas, m.mata_pelajaran FROM jadwal j JOIN kelas k ON j.kelas_id = k.id JOIN mapel m ON j.mapel_id = m.id JOIN guru g ON g.nip = j.nip";
  if (day || classroom || subject) {
    sqlQuery += " WHERE ";
  }
  if (day) {
    sqlQuery += "j.hari = ? ";
    values.push(day);
  }
  if (classroom) {
    if (values.length > 0) sqlQuery += "AND ";
    sqlQuery += "k.nama LIKE ? ";
    values.push(`%${classroom}%`);
  }
  if (subject) {
    if (values.length > 0) sqlQuery += "AND ";
    sqlQuery += "m.mata_pelajaran LIKE ? ";
    values.push(`%${subject}%`);
  }

  db.query(sqlQuery, values, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    return res.status(200).json({ data: result });
  });
};

const getScheduleDetail = (req, res) => {
  const sqlQuery =
    "SELECT j.id, j.hari, j.jam_mulai, j.jam_selesai, k.nama AS kelas, m.mata_pelajaran FROM jadwal j JOIN kelas k ON j.kelas_id = k.id JOIN mapel m ON j.mapel_id = m.id WHERE j.id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    return res.status(200).json({ data: result });
  });
};

const createNewSchedule = (req, res) => {
  const sqlQuery = "INSERT INTO jadwal SET ?";
  db.query(sqlQuery, req.body, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    if (result.affectedRows > 0)
      return res.status(201).json({
        msg: "Penambahan berhasil",
      });
    return res.status(500).json({ msg: "Penambahan gagal" });
  });
};

const editScheduleDetail = (req, res) => {
  const sqlQuery = "UPDATE jadwal SET ? WHERE id = ?";
  db.query(sqlQuery, [req.body, req.params.id], (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Edit berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

const deleteSchedule = (req, res) => {
  const sqlQuery = "DELETE FROM jadwal WHERE id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Hapus berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

module.exports = {
  getSchedule,
  getScheduleDetail,
  createNewSchedule,
  editScheduleDetail,
  deleteSchedule,
};
