const db = require("../config/mysql");

const getYear = (req, res) => {
  const semester = req.query.semester || null;
  const values = [];
  let sqlQuery = "SELECT id, tahunajar, semester FROM tahunajar";
  if (semester) {
    sqlQuery += " WHERE semester = ?";
    values.push(semester);
  }
  db.query(sqlQuery, values, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    return res.status(200).json({ data: result });
  });
};

const createNewYear = (req, res) => {
  const sqlQuery = "INSERT INTO tahunajar SET ?";
  db.query(sqlQuery, req.body, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    if (result.affectedRows > 0)
      return res.status(201).json({
        msg: "Penambahan berhasil",
      });
    return res.status(500).json({ msg: "Penambahan gagal" });
  });
};

const editYear = (req, res) => {
  const sqlQuery = "UPDATE tahunajar SET ? WHERE id = ?";
  db.query(sqlQuery, [req.body, req.params.id], (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Edit berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

const deleteYear = (req, res) => {
  const sqlQuery = "DELETE FROM tahunajar WHERE id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Hapus berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

module.exports = {
  getYear,
  createNewYear,
  editYear,
  deleteYear,
};
