const jwt = require("jsonwebtoken");
const db = require("../config/mysql");

const login = (req, res) => {
  const { body } = req;
  // cek db
  db.query(
    "SELECT id, password, role FROM user WHERE username = ? AND deleted = 0",
    body.username,
    (err, result) => {
      if (err)
        return res.status(500).json({
          msg: "Internal Server Error",
          err,
        });
      if (result[0].password === body.password) {
        const idType =
          result[0].role === "siswa"
            ? "nis"
            : result[0].role === "guru"
            ? "nip"
            : null;
        const payload = {
          id: result[0].id,
          username: body.username,
          role: result[0].role,
          idType,
        };
        const token = jwt.sign(payload, process.env.SECRET_KEY);
        return res.status(200).json({
          msg: "Berhasil login",
          token,
        });
      }
      return res.status(401).json({
        msg: "Invalid username or password",
      });
    }
  );
};

const logout = (req, res) => {
  const { body } = req;
  if (!body.token) {
    return res.status(400).json({
      msg: "Invalid Body",
    });
  }
  const token = body.token.split(" ")[1];
  jwt.verify(token, process.env.SECRET_KEY, (err) => {
    if (err) {
      return res.status(403).json({
        msg: "Failed",
      });
    }
    return res.status(200).json({
      msg: "Success",
    });
  });
};

module.exports = { login, logout };
