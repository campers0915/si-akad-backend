const db = require("../config/mysql");
const jwt = require("jsonwebtoken");

const authorization = (cb) => {};

const getProfile = (req, res) => {
  const bearerToken = req.header("Authorization");
  const token = bearerToken.split(" ")[1];
  if (!token) {
    return res.status(403).json({
      msg: "Silahkan login terlebih dahulu",
    });
  }
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    if (err) {
      return res.status(400).json({
        msg: "Silahkan login kembali",
      });
    }
    if (!payload.idType) {
      return res.status(400).json({
        msg: "Silahkan login dengan akun siswa/guru",
      });
    }
    db.query(
      `SELECT nama, jenis_kelamin, tempat_lahir, tanggal_lahir FROM ${payload.role} WHERE ${payload.idType} = ?`,
      payload.username,
      (err, result) => {
        if (err) return res.status(500).json({ msg: "Internal Server Error" });
        if (result.length === 0)
          return res.status(404).json({
            msg: "Data not found",
          });
        return res.status(200).json({ data: result });
      }
    );
  });
};
const editProfile = (req, res) => {
  const bearerToken = req.header("Authorization");
  const token = bearerToken.split(" ")[1];
  if (!token) {
    return res.status(403).json({
      msg: "Silahkan login terlebih dahulu",
    });
  }
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    if (err) {
      return res.status(400).json({
        msg: "Silahkan login kembali",
      });
    }
    if (!payload.idType) {
      return res.status(400).json({
        msg: "Silahkan login dengan akun siswa/guru",
      });
    }
    const editBody = Object.keys(req.body)
      .filter((key) => {
        const validKey = [
          "nama",
          "jenis_kelamin",
          "tempat_lahir",
          "tanggal_lahir",
        ];
        return validKey.includes(key);
      })
      .reduce(
        (current, key) =>
          Object.assign(current, {
            [key]:
              key === "tanggal_lahir" ? new Date(req.body[key]) : req.body[key],
          }),
        {}
      );
    // console.log({ ...editBody, updatedAt: new Date(Date.now()) });
    // return res.json({ msg: "OK" });
    db.query(
      `UPDATE ${payload.role} SET ? WHERE ${payload.idType} = ?`,
      [{ ...editBody, updatedAt: new Date(Date.now()) }, payload.username],
      (err, result) => {
        if (err) return res.status(500).json({ msg: "Internal Server Error" });
        if (result.affectedRows > 0)
          return res.status(200).json({ msg: "Edit berhasil" });
        return res.status(404).json({ msg: "Data tidak ditemukan" });
      }
    );
  });
};

module.exports = { getProfile, editProfile };
