const db = require("../config/mysql");

const getPresence = (req, res) => {
  const day = req.query.day || null;
  const clasroom = req.query.class || null;
  const subject = req.query.subject || null;
  const nis = req.query.nis || null;
  const values = [];
  let sqlQuery =
    "SELECT a.id, s.nama, a.tanggal, a.keterangan, k.nama AS kelas, m.mata_pelajaran FROM absensi a JOIN jadwal j ON a.jadwal_id = j.id JOIN kelas k ON j.kelas_id = k.id JOIN mapel m ON j.mapel_id = m.id JOIN siswa s ON a.nis = s.nis";
  if (day || clasroom || subject || nis) {
    sqlQuery += " WHERE ";
  }
  if (day) {
    sqlQuery += "j.hari LIKE ? ";
    values.push(day);
  }
  if (clasroom) {
    if (values.length > 0) sqlQuery += "AND ";
    sqlQuery += "k.nama LIKE ? ";
    values.push(`%${clasroom}%`);
  }
  if (subject) {
    if (values.length > 0) sqlQuery += "AND ";
    sqlQuery += "m.mata_pelajaran LIKE ? ";
    values.push(`%${subject}%`);
  }
  if (nis) {
    if (values.length > 0) sqlQuery += "AND ";
    sqlQuery += "a.nis = ? ";
    values.push(nis);
  }

  db.query(sqlQuery, values, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    if (result.length === 0)
      return res.status(404).json({ msg: "tidak ditemukan" });
    return res.status(200).json({ data: result });
  });
};

const createNewPresence = (req, res) => {
  const sqlQuery = "INSERT INTO absensi SET ?";
  db.query(sqlQuery, req.body, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    if (result.affectedRows > 0)
      return res.status(201).json({
        msg: "Penambahan berhasil",
      });
    return res.status(500).json({ msg: "Penambahan gagal" });
  });
};

const editPresenceDetail = (req, res) => {
  const sqlQuery = "UPDATE absensi SET ? WHERE id = ?";
  db.query(sqlQuery, [req.body, req.params.id], (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Edit berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

const deletePresence = (req, res) => {
  const sqlQuery = "DELETE FROM jadwal WHERE id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Hapus berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

module.exports = {
  getPresence,
  createNewPresence,
  editPresenceDetail,
  deletePresence,
};
