const db = require("../config/mysql");

const getSubject = (req, res) => {
  const subject = req.query.subject || null;
  let values = [];
  let sqlQuery = "SELECT m.id, m.mata_pelajaran FROM mapel m";
  if (subject) {
    sqlQuery += " WHERE m.mata_pelajaran LIKE ?";
    values.push(subject);
  }
  db.query(sqlQuery, values, (err, result) => {
    if (err)
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    return res.status(200).json({ data: result });
  });
};

const createNewSubject = (req, res) => {
  const sqlQuery = "INSERT INTO mapel SET ?";
  db.query(sqlQuery, req.body, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    if (result.affectedRows > 0)
      return res.status(201).json({
        msg: "Penambahan berhasil",
      });
    return res.status(500).json({ msg: "Penambahan gagal" });
  });
};

const editSubjectDetail = (req, res) => {
  const sqlQuery = "UPDATE mapel SET ? WHERE id = ?";
  db.query(sqlQuery, [req.body, req.params.id], (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Edit berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

const deleteSubject = (req, res) => {
  const sqlQuery = "DELETE FROM mapel WHERE id = ?";
  db.query(sqlQuery, req.params.id, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal Server Error" });
    if (result.affectedRows > 0)
      return res.status(200).json({ msg: "Hapus berhasil" });
    return res.status(404).json({ msg: "Data tidak ditemukan" });
  });
};

module.exports = {
  getSubject,
  createNewSubject,
  editSubjectDetail,
  deleteSubject,
};
