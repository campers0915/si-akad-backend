const db = require("../config/mysql");
const getAllUserByRole = (req, res) => {
  const { params } = req;
  if (!["siswa", "guru"].includes(params.role))
    return res.status(400).json({ msg: "invalid route" });
  const idType = params.role === "siswa" ? "nis" : "nip";
  let sqlQuery = `SELECT u.id, u.username, s.nama FROM user u 
  JOIN ${params.role} s ON u.username = s.${idType} WHERE deleted = 0`;
  db.query(sqlQuery, (err, result) => {
    console.log(result);
    if (err) return res.status(500).json({ msg: "Internal server error" });
    if (result.length === 0)
      return res.status(404).json({ msg: "Data not found" });
    return res.status(200).json({
      data: result,
    });
  });
};
const getUserById = (req, res) => {
  const { params } = req;
  if (!["siswa", "guru"].includes(params.role))
    return res.status(400).json({ msg: "invalid route" });
  const idType = params.role === "siswa" ? "nis" : "nip";
  let sqlQuery = `SELECT u.id, u.username, s.nama, s.jenis_kelamin, s.tempat_lahir, s.tanggal_lahir 
  FROM user u JOIN ${params.role} s ON u.username = s.${idType} WHERE u.username = ? AND deleted = 0`;
  db.query(sqlQuery, params.user, (err, result) => {
    if (err) return res.status(500).json({ msg: "Internal server error" });
    if (result.length === 0)
      return res.status(404).json({ msg: "Data not found" });
    return res.status(200).json({
      data: result,
    });
  });
};
const createNewUser = (req, res) => {
  const { body } = req;
  if (!["siswa", "guru"].includes(body.role)) {
    return res.status(400).json({ msg: "input salah" });
  }
  const userBody = {
    username: body.username,
    password: body.password,
    role: body.role,
  };
  const detailBody = {
    [body.role === "siswa" ? "nis" : body.role === "guru" && "nip"]:
      body.username,
    nama: body.name,
  };
  // console.log(userBody);
  // console.log(detailBody);
  // return res.json({ msg: "OK" });
  db.query("INSERT INTO user SET ?", userBody, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
        err,
      });
    }
    db.query(`INSERT INTO ${body.role} SET ?`, detailBody, (err, result) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          msg: "Internal server error",
          err,
        });
      }
      if (result.affectedRows > 0)
        return res.status(201).json({
          msg: "Penambahan berhasil",
        });
      return res.status(500).json({ msg: "Penambahan gagal" });
    });
  });
};
const editUserById = (req, res) => {
  const { body, params } = req;
  if (!["siswa", "guru"].includes(params.role)) {
    return res.status(400).json({ msg: "input salah" });
  }

  const editBody = Object.keys(body)
    .filter((key) =>
      ["nama", "jenis_kelamin", "tempat_lahir", "tanggal_lahir"].includes(key)
    )
    .reduce(
      (current, key) =>
        Object.assign(current, {
          [key]:
            key === "tanggal_lahir" ? new Date(req.body[key]) : req.body[key],
        }),
      {}
    );
  const idType = params.role === "siswa" ? "nis" : "nip";
  db.query(
    `UPDATE ${params.role} SET ? WHERE ${idType} = ?`,
    [{ ...editBody, updatedAt: new Date(Date.now()) }, params.user],
    (err, result) => {
      if (err) return res.status(500).json({ msg: "Internal Server Error" });
      if (result.affectedRows > 0)
        return res.status(200).json({ msg: "Edit berhasil" });
      return res.status(404).json({ msg: "Data tidak ditemukan" });
    }
  );
};
const deleteUserById = (req, res) => {
  const { params } = req;
  if (!["siswa", "guru"].includes(params.role)) {
    return res.status(400).json({ msg: "input salah" });
  }
  // const idType = params.role === "siswa" ? "nis" : "nip";
  // Soft Delete
  const queryValue = {
    deletedAt: new Date(Date.now()),
    deleted: 1,
  };
  db.query(
    "UPDATE user SET ? WHERE username = ? AND role = ?",
    [queryValue, params.user, params.role],
    (err, result) => {
      if (err) return res.status(500).json({ msg: "Internal Server Error" });
      if (result.affectedRows === 0)
        return res.status(404).json({ msg: "Data tidak ditemukan" });
      return res.status(200).json({ msg: "Hapus berhasil" });
    }
  );

  // Hard Delete
  // db.query(
  //   `DELETE FROM ${params.role} WHERE ${idType} = ?`,
  //   params.user,
  //   (err, result) => {
  //     if (err) return res.status(500).json({ msg: "Internal Server Error" });
  //     if (result.affectedRows === 0)
  //       return res.status(404).json({ msg: "Data tidak ditemukan" });
  //     db.query(
  //       "DELETE FROM user WHERE username = ? AND role = ?",
  //       [params.user, params.role],
  //       (err) => {
  //         if (err)
  //           return res.status(500).json({ msg: "Internal Server Error" });
  //         return res.status(200).json({ msg: "Hapus berhasil" });
  //       }
  //     );
  //   }
  // );
};

module.exports = {
  getAllUserByRole,
  getUserById,
  createNewUser,
  editUserById,
  deleteUserById,
};
