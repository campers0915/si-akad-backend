require("dotenv").config();
const express = require("express");

const Router = require("./src/router");

const server = express();

const PORT = process.env.PORT || 8080;

server.use(express.urlencoded({ extended: false }));
server.use(express.json());

server.use(Router);

server.listen(PORT, () => {
  console.log("Server is Running at PORT", PORT);
});
